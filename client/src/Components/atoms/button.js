import React from "react";

function Button({ text, onClick, type, className }) {
  return (
    <button className={className} type={type} onClick={onClick}>
      {text}
    </button>
  );
}

export default Button;
